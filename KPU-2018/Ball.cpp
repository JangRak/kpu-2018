#include "Ball.h"
#include <iostream>
#include "GoalNet.h"
#include <ctime>

enum SPINNING { SPIN, NONSPIN, NONE_SELECT_SPINNING };
enum DIRECTION { LEFT, CENTER, RIGHT };
enum FOOT { LEFT_FOOT, RIGHT_FOOT, NONE_SELECT_FOOT };
enum WORK { ON, OFF, NONE };
enum COLLIDE { YES, NO };

Ball::Ball() : D(-100.0, -135.0, 700.0), Work(OFF), Shooting_Power(0.0), Time(0.0), Velocity(0.0), Dir_Value(0.0), Dir_Power(0.0), Trans_xPos(-100.0), 
			   Collision_GoalNet(NO), Collision_Ground(NO), Trans_yPos(-135.0), Trans_zPos(700.0), Time_for_Collision_Ground(0.015), Kicking_Press(NONE),
			   Spinning(NONSPIN), Main_Foot(RIGHT_FOOT), Collision_Post_Up(NO), Collision_Post_Left(NO), Collision_Post_Right(NO),
			   Can_Shoot(OFF) {}

Ball::~Ball() {}

void Ball:: Update()
{
	PIXEL_PER_METER = (10.0 / 0.3);
	BALL_SPEED_KMPH = Shooting_Power / 40.0;
	BALL_SPEED_MPM = (BALL_SPEED_KMPH * 1000.0 / 60.0);
	BALL_SPEED_MPS = (BALL_SPEED_MPM / 60.0);
	BALL_SPEED_PPS = (BALL_SPEED_KMPH * PIXEL_PER_METER);

	DIR_SPEED_KMPH = Dir_Power / 50.0;
	DIR_SPEED_MPM = (DIR_SPEED_KMPH * 1000.0 / 60.0);
	DIR_SPEED_MPS = (DIR_SPEED_MPM / 60.0);
	DIR_SPEED_PPS = (DIR_SPEED_KMPH * PIXEL_PER_METER);
}

void Ball::Draw()
{
	if (Can_Shoot == ON) {
		if (Collision_Post_Up == YES || Collision_Post_Left == YES || Collision_Post_Right == YES) {
			xPos_for_Collision_Post = Trans_xPos;
			yPos_for_Collision_Post = Trans_yPos;
			zPos_for_Collision_Post = Trans_zPos;
		}

		glPushMatrix();									// 공그리기
		if (Work == ON && Shooting_Power > 20.0) {
			// 발, 위치 적용하자.
			if (Collision_Post_Up == NO && Collision_Post_Left == NO && Collision_Post_Right == NO) {
				if (Collision_Ground == NO) {
					if (Collision_GoalNet == NO) {
						if (Spinning == SPIN) {
							if (Main_Foot == RIGHT_FOOT) {
								Trans_xPos = -(0.9 * Gravity * Time*Time) + ((BALL_SPEED_PPS + 4.0) * Time) + D.xPos;
								Trans_yPos = -(0.9 * Gravity * Time*Time) + ((BALL_SPEED_PPS + 4.0) * Time) + D.yPos;
								Trans_zPos = -((BALL_SPEED_PPS + 4.0) * Time * 2 - (D.zPos));
							}
							else if (Main_Foot == LEFT_FOOT) {
								Trans_xPos = -(-(0.9 * Gravity * Time*Time) + ((BALL_SPEED_PPS + 4.0) * Time)) + D.xPos;
								Trans_yPos = -(0.9 * Gravity * Time*Time) + ((BALL_SPEED_PPS + 4.0) * Time) + D.yPos;
								Trans_zPos = -((BALL_SPEED_PPS + 4.0) * Time * 2 - (D.zPos));
							}
						}
						else if (Spinning == NONSPIN) {
							Trans_xPos = (DIR_SPEED_PPS + 4.0) * Time + (D.xPos);
							Trans_yPos = -(0.9 * Gravity * Time*Time) + ((BALL_SPEED_PPS + 4.0) * Time) + D.yPos;
							Trans_zPos = -((BALL_SPEED_PPS + 4.0) * Time * 2 - (D.zPos));
						}
					}
					// 네트에 닿으면
					else if (Collision_GoalNet == YES) {
						Trans_yPos -= 0.9 *  Gravity * BALL_SPEED_PPS*0.005 * BALL_SPEED_PPS*0.005;
						Trans_zPos += BALL_SPEED_PPS * 0.006;
					}
				}
				else if (Collision_Ground == YES) {
					if (Collision_GoalNet == YES) {
						if (Collision_Ground == YES)
							Trans_yPos = -(0.9 * Gravity * Time_for_Collision_Ground*Time_for_Collision_Ground) + ((BALL_SPEED_PPS + 4.0) * Time_for_Collision_Ground) + yPos_for_Collision_Ground;
						else
							Trans_yPos -= 0.9 *  Gravity * BALL_SPEED_PPS*0.002 * BALL_SPEED_PPS*0.002;

						Trans_zPos += BALL_SPEED_PPS * 0.01;
					}
					else {
						//std::cout << "이거실행" << std::endl;
						Trans_yPos = -(0.9 * Gravity * Time_for_Collision_Ground*Time_for_Collision_Ground) + ((BALL_SPEED_PPS + 4.0) * Time_for_Collision_Ground) + yPos_for_Collision_Ground;
						Trans_zPos = -((BALL_SPEED_PPS + 4.0) * Time_for_Collision_Ground * 2 - (zPos_for_Collision_Ground));
					}
					Trans_xPos = (DIR_SPEED_PPS + 4.0) * Time_for_Collision_Ground + (xPos_for_Collision_Ground);

					BALL_SPEED_PPS = BALL_SPEED_PPS * 0.9995;
					//std::cout << Trans_xPos << std::endl;
					//std::cout << Trans_yPos << std::endl;
					//std::cout << Trans_zPos << std::endl;
					//std::cout << Time_for_Collision_Ground << std::endl;

					Time_for_Collision_Ground += 0.015;
				}
			}
			else if (Collision_Post_Up == YES || Collision_Post_Left == YES || Collision_Post_Right == YES) {
				if (Collision_Ground == NO) {
					if (Spinning == SPIN) {
						if (Main_Foot == RIGHT_FOOT) {
							Trans_xPos = -(0.9 * Gravity * Time*Time) + ((BALL_SPEED_PPS + 4.0) * Time) + xPos_for_Collision_Post;
							Trans_yPos = -(0.9 * Gravity * Time*Time) + ((BALL_SPEED_PPS + 4.0) * Time) + yPos_for_Collision_Post;
							Trans_zPos += BALL_SPEED_PPS * 0.001;
						}
						else if (Main_Foot == LEFT_FOOT) {
							Trans_xPos = -(-(0.9 * Gravity * Time*Time) + ((BALL_SPEED_PPS + 4.0) * Time)) + xPos_for_Collision_Post;
							Trans_yPos = -(0.9 * Gravity * Time*Time) + ((BALL_SPEED_PPS + 4.0) * Time) + yPos_for_Collision_Post;
							Trans_zPos += BALL_SPEED_PPS * 0.001;
						}
					}
					else if (Spinning == NONSPIN) {
						Trans_xPos = (DIR_SPEED_PPS + 4.0) * Time + (xPos_for_Collision_Post);
						Trans_yPos = -(0.9 * Gravity * Time*Time) + ((BALL_SPEED_PPS + 4.0) * Time) + yPos_for_Collision_Post;
						Trans_zPos += BALL_SPEED_PPS * 0.001;
					}
				}
				else if (Collision_Ground == YES) {
					if (Collision_Ground == YES)
						Trans_yPos = -(0.9 * Gravity * Time_for_Collision_Ground*Time_for_Collision_Ground) + ((BALL_SPEED_PPS + 4.0) * Time_for_Collision_Ground) + yPos_for_Collision_Ground;
					else
						Trans_yPos -= 0.9 *  Gravity * BALL_SPEED_PPS*0.002 * BALL_SPEED_PPS*0.002;

					Trans_zPos += BALL_SPEED_PPS * 0.01;
					Trans_xPos = (DIR_SPEED_PPS + 4.0) * Time + (D.xPos);

					BALL_SPEED_PPS = BALL_SPEED_PPS * 0.9995;
					//std::cout << Trans_xPos << std::endl;
					//std::cout << Trans_yPos << std::endl;
					//std::cout << Trans_zPos << std::endl;
					//std::cout << Time_for_Collision_Ground << std::endl;

					Time_for_Collision_Ground += 0.015;
				}
			}
		}

		//std::cout << Shooting_Power << std::endl;
		//std::cout << Spinning << std::endl;
		//std::cout << D.yPos << std::endl;
		//std::cout << D.zPos << std::endl;

		glTranslated(Trans_xPos, Trans_yPos, Trans_zPos);

		if (Spinning == SPIN)
			glRotated(D.RotateValue, D.Rotate_X, D.Rotate_Y, D.Rotate_Z);

		glLineWidth(2.0);

		glColor3d(D.Red, D.Green, D.Blue);

		glutSolidSphere(15, 30, 30);

		glColor3d(0.0, 0.0, 0.0);
		glutWireSphere(18, 10, 10);

		glPopMatrix();									// 공그리기

		if (Trans_zPos < D.zPos && Trans_yPos < D.yPos && Work == ON) {
			//std::cout << "Work" << std::endl;
			Collision_Ground = YES;
			xPos_for_Collision_Ground = Trans_xPos;
			yPos_for_Collision_Ground = Trans_yPos;
			zPos_for_Collision_Ground = Trans_zPos;
			Time_for_Collision_Ground = 0.015;
		}
	}

	else {

		glPushMatrix();									// 공그리기

			glTranslated(D.xPos, D.yPos, D.zPos);

			glLineWidth(2.0);

			glColor3d(D.Red, D.Green, D.Blue);

			glutSolidSphere(15, 30, 30);

			glColor3d(0.0, 0.0, 0.0);
			glutWireSphere(18, 10, 10);

		glPopMatrix();

	}

}

// 초기화
void Ball::Init()
{
	//srand((unsigned)time(nullptr));

	D.xPos = (double)((rand() % 20 - 10) * 10);
	Trans_xPos = D.xPos;
	Trans_yPos = D.yPos + 0.0001;
	Trans_zPos = D.zPos;
	D.Rotate_X = D.Rotate_Y = D.Rotate_Z = D.RotateValue = 0.0;

	Time = 0.0;
	Time_for_Collision_Ground = 0.015;

	Dir_Power = 0.0;
	Shooting_Power = 0.0;
	Kicking_Press = NONE;
	Work = OFF;

	Can_Shoot = OFF;

	Collision_Post_Up = OFF;
	Collision_Post_Left = OFF;
	Collision_Post_Right = OFF;

	Spinning = NONSPIN;
	Main_Foot = RIGHT_FOOT;

	Collision_GoalNet = NO;
	Collision_Ground = NO;
}

// 회전 여부
void Ball::Set_Spinning(int state) { Spinning = state; }

// 변화값
void Ball::Set_xPos(double xPos) { D.xPos += xPos; }
void Ball::Set_yPos(double yPos) { D.yPos += yPos; }
void Ball::Set_zPos(double zPos) { D.zPos += zPos; }

// 회전값
void Ball::Set_Rotate(double RotateValue, int Foot)
{
	if (Foot == LEFT_FOOT) {
		D.RotateValue += RotateValue;
		D.Rotate_X = D.Rotate_Y = 1.0;
	}
	else if (Foot == RIGHT_FOOT) {
		D.RotateValue += RotateValue;
		D.Rotate_X = -1.0;
		D.Rotate_Y = 1.0;
	}
}

// 방향값
void Ball::Set_Dir_Value(double Value) { Dir_Value += Value; }

// 슈팅세기
void Ball::Set_Shooting_Power() 
{ 
	Shooting_Power += 4.0; 
	if (Shooting_Power > 200.0)
		Shooting_Power = 200.0;
}

void Ball::Shooting_Power_Init() { Shooting_Power = 0.0; }

void Ball::Set_Dir_Power(double Value)
{
	Dir_Power += Value;
	if (Dir_Power > 150.0)
		Dir_Power = 150.0;

	if (Dir_Power < -150.0)
		Dir_Power = -150.0;
}

// x(t) = Vx * t + Sx
// y(t) = -(0.5 * g * t^2) + (Vy * t) + Sy
// z(t) = Vz * t + Sz

// S : 초기위치 / V : 초기속도
// g : 중력가속도 / t : 시간