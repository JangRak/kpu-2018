#include "GoalKeeper.h"

enum BEHAVIOR { KEEPER_IDLE, DIVING, MOVE, JUMP, DIVING_UP };

GoalKeeper::GoalKeeper() : Situation(KEEPER_IDLE), D(0.0, -50.0, -650.0, 0.0), Dive_Direction(false), Jump_State(0), Leg_Bend_State(false), Leg_Side_State(false), Leg_Rotate_X(0.0), Leg_Rotate_Z(0.0), RightHand_Move_Y(0.0), LeftHand_Move_Y(0.0), RightHand_Rotate_Z(30.0), LeftHand_Rotate_Z(-30.0) {}

GoalKeeper::~GoalKeeper()
{

}
void GoalKeeper::Init() {
	Situation = KEEPER_IDLE;
	D.xPos = 0.0;
	D.yPos = -50.0;
	D.zPos = -650.0;
	D.Rotate_Z = 0.0;
	Dive_Direction = false;
	Jump_State = 0;
	Leg_Bend_State = false;
	Leg_Side_State = false;
	Leg_Rotate_X = 0;
	Leg_Rotate_Z = 0;
	RightHand_Move_Y = 0;
	LeftHand_Move_Y = 0;
	RightHand_Rotate_Z = 30.0;
	LeftHand_Rotate_Z = -30.0;
}

void GoalKeeper::Diving_Left() {
	if (Situation == MOVE || Situation == KEEPER_IDLE) {
		Situation = DIVING;
		Jump_State = 0;
		Leg_Bend_State = false;
		Dive_Direction = false;
	}
	else if (Situation == JUMP && Jump_State == 0) {
		Situation = DIVING_UP;
		Dive_Direction = false;
	}
}
void GoalKeeper::Diving_Right() {
	if (Situation == MOVE || Situation == KEEPER_IDLE) {
		Situation = DIVING;
		Jump_State = 0;
		Leg_Bend_State = false;
		Dive_Direction = true;
	}
	else if (Situation == JUMP && Jump_State == 0) {
		Situation = DIVING_UP;
		Dive_Direction = true;
	}
}
void GoalKeeper::Move_Left() {
	if (Situation == KEEPER_IDLE || Situation == MOVE) {
		D.xPos -= 8.0;
		Situation = MOVE;
	}
}
void GoalKeeper::Move_Right() {
	if (Situation == KEEPER_IDLE || Situation == MOVE) {
		D.xPos += 8.0;
		Situation = MOVE;
	}
}
void GoalKeeper::Jump() {
	//�ƿ� ��������
	if (Situation == KEEPER_IDLE || Situation == MOVE) {
		Situation = JUMP;
		Jump_State = 0;
		Leg_Bend_State = false;
	}
	else if (Situation == DIVING && Jump_State == 0) {
		Situation = DIVING_UP;
	}
}
void GoalKeeper::Time() {
	if (Situation == KEEPER_IDLE) {

		/*if (Leg_Side_State == false) {
		if (Leg_Rotate_Z < 20.0) {
		Leg_Rotate_Z += 0.1;
		}
		else {
		Leg_Side_State = true;
		}
		}
		else {
		if (Leg_Rotate_Z > 0.0) {
		Leg_Rotate_Z -= 0.1;
		}
		else {
		Leg_Side_State = false;
		}
		}*/

		//�⺻ ��������, ���θ��� ����
		if (Leg_Bend_State == false) {
			if (Leg_Rotate_X < 20.0) {
				Leg_Rotate_X += 0.1;
			}
			else {
				Leg_Bend_State = true;
			}
		}
		//�⺻ ��������, �Ǵ� ����
		else if (Leg_Bend_State == true) {
			if (Leg_Rotate_X > 0.0) {
				Leg_Rotate_X -= 0.1;
			}
			else {
				Leg_Bend_State = false;
			}
		}
		//D.yPos = -50.0 - sin(Leg_Rotate_X*3.14 / 180.0) * 10; �̰� ����� �� ��ü�� ���Ʒ��� ������
		/*else if (LeftHand_Rotate_Z > 10) {
		LeftHand_Rotate_Z--;
		}*/
	}
	else if (Situation == DIVING) {
		if (Jump_State == 0) {
			if (Leg_Bend_State == false) {
				if (Leg_Rotate_X < 45.0) {
					Leg_Rotate_X += 0.5;
				}
				else {
					Leg_Bend_State = true;
				}
			}
			else {
				if (Leg_Rotate_X > 0.0) {
					Leg_Rotate_X -= 0.5;
				}
				else {
					Leg_Bend_State = false;
					Leg_Rotate_X = 0.0;
					Jump_State = 1;
				}
			}
		}
		else if (Jump_State == 1) {
			//������
			if (Dive_Direction) {
				D.xPos += 1.2;
				D.yPos += 0.4;

				if (D.yPos >= -40.0) {
					Jump_State = 2;
				}

				if (D.Rotate_Z > -90.0) {
					D.Rotate_Z -= 0.5;
					//Jump_State = 2;
				}
				if (RightHand_Rotate_Z < 180.0) {
					//LeftHand_Rotate_Z -= 0.8;
					RightHand_Rotate_Z += 0.8;
				}
			}
			//����
			else {
				D.xPos -= 1.2;
				D.yPos += 0.4;

				if (D.yPos >= -40.0) {
					Jump_State = 2;
				}

				if (D.Rotate_Z < 90.0) {
					D.Rotate_Z += 0.5;
					//Jump_State = 2;
				}
				if (LeftHand_Rotate_Z > -180.0) {
					LeftHand_Rotate_Z -= 0.8;
					//RightHand_Rotate_Z += 0.8;
				}
			}
		}
		else if (Jump_State == 2) {
			//������
			if (Dive_Direction) {
				D.xPos += 1.2;
				D.yPos -= 0.6;

				if (D.yPos <= -123.0) {
					Jump_State = 3;
				}

				if (D.Rotate_Z > -90.0) {
					D.Rotate_Z -= 0.8;
					//Jump_State = 2;
				}
				if (RightHand_Rotate_Z < 180.0) {
					//LeftHand_Rotate_Z -= 0.8;
					RightHand_Rotate_Z += 1.6;
				}
			}
			//����
			else {
				D.xPos -= 1.2;
				D.yPos -= 0.6;

				if (D.yPos <= -123.0) {
					Jump_State = 3;
				}

				if (D.Rotate_Z < 90.0) {
					D.Rotate_Z += 0.8;
					//Jump_State = 2;
				}
				if (LeftHand_Rotate_Z > -180.0) {
					LeftHand_Rotate_Z -= 1.6;
					//RightHand_Rotate_Z += 0.8;
				}
			}
		}
		//else {
		//   //������
		//   if (Dive_Direction) {
		//      D.Rotate_Z += 0.5;
		//      if (D.Rotate_Z > 0.0) {
		//         Jump_State = 0;
		//         D.Rotate_Z = 0.0;
		//         Situation = IDLE;
		//      }
		//      if (RightHand_Rotate_Z > 30.0) {
		//         //LeftHand_Rotate_Z += 0.8;
		//         RightHand_Rotate_Z -= 0.8;
		//      }
		//   }
		//   //����
		//   else {
		//      D.Rotate_Z -= 0.5;
		//      if (D.Rotate_Z < 0.0) {
		//         Jump_State = 0;
		//         D.Rotate_Z = 0.0;
		//         Situation = IDLE;
		//      }
		//      if (LeftHand_Rotate_Z < -30.0) {
		//         LeftHand_Rotate_Z += 0.8;
		//         //RightHand_Rotate_Z -= 0.8;
		//      }
		//   }
		//}
	}
	else if (Situation == MOVE) {
		if (Leg_Side_State == false) {
			if (Leg_Rotate_Z < 20.0) {
				Leg_Rotate_Z += 5.0;
			}
			else {
				Leg_Side_State = true;
			}
		}
		else {
			if (Leg_Rotate_Z > 0.0) {
				Leg_Rotate_Z -= 5.0;
			}
			else {
				Leg_Side_State = false;

			}
		}
		Situation = KEEPER_IDLE;
	}
	else if (Situation == JUMP) {
		//�����غ�
		if (Jump_State == 0) {
			if (Leg_Bend_State == false) {
				if (Leg_Rotate_X < 45.0) {
					Leg_Rotate_X += 0.5;
				}
				else {
					Leg_Bend_State = true;
				}
			}
			else {
				if (Leg_Rotate_X > 0.0) {
					Leg_Rotate_X -= 0.5;
				}
				else {
					Leg_Bend_State = false;
					Leg_Rotate_X = 0.0;
					Jump_State = 1;
				}
			}
		}
		//����ϴ� ����
		else if (Jump_State == 1) {
			D.yPos += 0.5;
			if (D.yPos >= 8.0) {
				Jump_State = 2;
			}
			if (LeftHand_Rotate_Z > -180.0) {
				LeftHand_Rotate_Z -= 1.6;
				RightHand_Rotate_Z += 1.6;
			}
		}
		//������ �϶��ϴ� ����
		else {
			D.yPos -= 0.5;
			if (D.yPos <= -50.0) {
				Situation = KEEPER_IDLE;
				Jump_State = 0;
				D.yPos = -50.0;
				LeftHand_Rotate_Z = -30.0;
				RightHand_Rotate_Z = 30.0;
			}
			if (LeftHand_Rotate_Z < -30.0) {
				LeftHand_Rotate_Z += 1.6;
				RightHand_Rotate_Z -= 1.6;
			}
		}
	}
	else if (Situation == DIVING_UP) {
		if (Jump_State == 0) {
			if (Leg_Bend_State == false) {
				if (Leg_Rotate_X < 45.0) {
					Leg_Rotate_X += 0.5;
				}
				else {
					Leg_Bend_State = true;
				}
			}
			else {
				if (Leg_Rotate_X > 0.0) {
					Leg_Rotate_X -= 0.5;
				}
				else {
					Leg_Bend_State = false;
					Leg_Rotate_X = 0.0;
					Jump_State = 1;
				}
			}
		}
		else if (Jump_State == 1) {
			//������
			if (Dive_Direction) {
				D.xPos += 1.5;
				D.yPos += 0.7;

				if (D.yPos >= 10.0) {
					Jump_State = 2;
				}

				if (D.Rotate_Z > -60.0) {
					D.Rotate_Z -= 0.5;
					//Jump_State = 2;
				}
				if (RightHand_Rotate_Z < 180.0) {
					//LeftHand_Rotate_Z -= 0.8;
					RightHand_Rotate_Z += 0.8;
				}
			}
			//����
			else {
				D.xPos -= 1.5;
				D.yPos += 0.7;

				if (D.yPos >= 10.0) {
					Jump_State = 2;
				}

				if (D.Rotate_Z < 60.0) {
					D.Rotate_Z += 0.5;
					//Jump_State = 2;
				}
				if (LeftHand_Rotate_Z > -180.0) {
					LeftHand_Rotate_Z -= 0.8;
					//RightHand_Rotate_Z += 0.8;
				}
			}
		}
		else if (Jump_State == 2) {
			if (Dive_Direction) {
				D.xPos += 1.0;
				D.yPos -= 1.0;

				if (D.yPos <= -123.0) {
					Jump_State = 3;
					//Situation = IDLE;
				}

				if (D.Rotate_Z > -90.0) {
					D.Rotate_Z -= 0.5;
					//Jump_State = 2;
				}
				if (RightHand_Rotate_Z < 180.0) {
					//LeftHand_Rotate_Z -= 0.8;
					RightHand_Rotate_Z += 0.8;
				}
			}
			else {
				D.xPos -= 1.0;
				D.yPos -= 1.0;

				if (D.yPos <= -123.0) {
					Jump_State = 3;
					//Situation = IDLE;
				}

				if (D.Rotate_Z < 90.0) {
					D.Rotate_Z += 0.5;
					//Jump_State = 2;
				}
				if (LeftHand_Rotate_Z > -180.0) {
					LeftHand_Rotate_Z -= 0.8;
					//RightHand_Rotate_Z += 0.8;
				}
			}
		}
	}
}
void GoalKeeper::Draw() {
	glPushMatrix(); // �Ӹ�
	glTranslated(D.xPos, D.yPos, D.zPos);
	glRotated(D.Rotate_Z, 0.0, 0.0, 1.0);
	//  �⺻ ��������

	glPushMatrix();
	glTranslated(0.0, sin(-Leg_Rotate_X * 3.14 / 180.0) * 10, 0.0); // �̰� ����� ���� ���� ������
	glColor3d(0.6, 0.6, 0.2);
	glutSolidSphere(10.0, 10, 10);

	glPushMatrix();//��
	glColor3d(0.8, 0.8, 0.5);
	glTranslated(0.0, -9.0, 0.0);
	glRotated(90.0, 1.0, 0.0, 0.0);
	glutSolidCylinder(3.0, 5.0, 10, 10);
	glPopMatrix();//��

	glPushMatrix();//��
	glColor3d(0.8, 0.3, 0.0);
	glTranslated(0.0, -33.0, 0.0);
	glScaled(1.5, 2.0, 1.0);
	glutSolidCube(20.0);
	glPopMatrix();//��

	glPushMatrix(); // ����
	glColor3d(0.0, 0.0, 0.8);
	glTranslated(-15.0, -15.0, 0.0);
	glRotated(LeftHand_Rotate_Z, 0.0, 0.0, 1.0);
	glPushMatrix();// ���� �� ����
	glRotated(90.0, 1.0, 0.0, 0.0);
	glutSolidCylinder(4.0, 20.0, 10, 10);
	glPopMatrix();// ���� �� ����
	glTranslated(0.0, -20.0, 0.0);
	glRotated(-180.0 - LeftHand_Rotate_Z, 0.0, 0.0, 1.0);
	glRotated(90.0, 1.0, 0.0, 0.0);
	glutSolidCylinder(4.0, 20.0, 10, 10);
	glPopMatrix(); // ����

	glPushMatrix(); // ������
	glColor3d(0.0, 0.0, 0.8);
	glTranslated(15.0, -15.0, 0.0);
	glRotated(RightHand_Rotate_Z, 0.0, 0.0, 1.0);
	glPushMatrix();// ������ �� ����
	glRotated(90.0, 1.0, 0.0, 0.0);
	glutSolidCylinder(4.0, 20.0, 10, 10);
	glPopMatrix();// ������ �� ����
	glTranslated(0.0, -20.0, 0.0);
	glRotated(180.0 - RightHand_Rotate_Z, 0.0, 0.0, 1.0);
	glRotated(90.0, 1.0, 0.0, 0.0);
	glutSolidCylinder(4.0, 20.0, 10, 10);
	glPopMatrix(); // ������

	glPushMatrix();//�޴ٸ� �����
	glColor3d(0.0, 0.8, 0.8);
	glTranslated(-5.0, -53.0, 0.0);
	glRotated(-Leg_Rotate_Z, 0.0, 0.0, 1.0);
	glRotated(90.0 - Leg_Rotate_X, 1.0, 0.0, 0.0);
	glutSolidCylinder(3.0, 20.0, 10, 10);
	glPopMatrix();//�޴ٸ� �����

	glPushMatrix();//�����ٸ� �����
	glColor3d(0.0, 0.8, 0.8);
	glTranslated(-5.0, -53.0, 0.0);
	glRotated(-Leg_Rotate_Z, 0.0, 0.0, 1.0);
	glRotated(90.0 - Leg_Rotate_X, 1.0, 0.0, 0.0);
	glutSolidCylinder(3.0, 20.0, 10, 10);
	glPopMatrix();//�����ٸ� �����

	glPushMatrix();//�����ٸ� �����
	glColor3d(0.0, 0.8, 0.8);
	glTranslated(5.0, -53.0, 0.0);
	glRotated(Leg_Rotate_Z, 0.0, 0.0, 1.0);
	glRotated(90.0 - Leg_Rotate_X, 1.0, 0.0, 0.0);
	glutSolidCylinder(3.0, 20.0, 10, 10);
	glPopMatrix();//�����ٸ� �����

	glPopMatrix();

	glPushMatrix();// �޴ٸ� ���Ƹ�
	glTranslated(-5.0, -53.0, 0.0);
	glRotated(-Leg_Rotate_Z, 0.0, 0.0, 1.0);
	glTranslated(0.0, -40.0, 0.0);
	glRotated(-90.0 + Leg_Rotate_X, 1.0, 0.0, 0.0);
	glutSolidCylinder(3.0, 20.0, 10, 10);
	glPopMatrix();// �޴ٸ� ���Ƹ�

	glPushMatrix();// �����ٸ� ���Ƹ�
	glTranslated(5.0, -53.0, 0.0);
	glRotated(Leg_Rotate_Z, 0.0, 0.0, 1.0);
	glTranslated(0.0, -40.0, 0.0);
	glRotated(-90.0 + Leg_Rotate_X, 1.0, 0.0, 0.0);
	glutSolidCylinder(3.0, 20.0, 10, 10);
	glPopMatrix();// �����ٸ� ���Ƹ�

	glPopMatrix(); //�Ӹ�
}