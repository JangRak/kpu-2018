#pragma once
#include "SharedData.h"

class Ground
{
public:
	Ground();
	~Ground();

public :
	void Draw();

private :
	Data D;
};

