#pragma once
#include "SharedData.h"
class GoalKeeper
{
public:
	GoalKeeper();
	~GoalKeeper();
public:
	void Draw();
	void Time();
	void Move_Left();
	void Move_Right();
	void Jump();
	void Diving_Left();
	void Diving_Right();
	void Init();
	double Get_X() { return D.xPos; }
	double Get_Y() { return D.yPos; }
	double Get_Z() { return D.zPos; }
	double Get_Zangle() { return D.Rotate_Z; }
	int Get_JumpState() { return Jump_State; }
	int Get_Situation() { return Situation; }
	bool Get_Dive_Direct() { return Dive_Direction; }
private:
	Data D;
	double Leg_Rotate_X;
	double RightHand_Move_Y;
	double LeftHand_Move_Y;
	double RightHand_Rotate_Z;
	double LeftHand_Rotate_Z;
	double Leg_Rotate_Z;
	bool Leg_Side_State;
	bool Leg_Bend_State;
	int Jump_State;
	bool Dive_Direction; // false 는 왼쪽 true 는 오른쪽
	int Situation;
};
