#include "Ground.h"

Ground::Ground() : D(1000.0, 1000.0, 90.0, 1.0, 0.3, 1.0, 0.0) {}
Ground::~Ground() {}

void Ground::Draw()
{
	glPushMatrix();	// �ٴ�

	glPointSize(100.0);
	glLineWidth(10.0);

	glBegin(GL_LINE_STRIP);
	glColor3d(1.0, 1.0, 1.0);

	glVertex3d(-400.0, -150.0, -900.0);
	glVertex3d(-400.0, -150.0, -500.0);
	glVertex3d(400.0, -150.0, -500.0);
	glVertex3d(400.0, -150.0, -900.0);
	glEnd();

	glBegin(GL_LINE_STRIP);
	glColor3d(1.0, 1.0, 1.0);

	glVertex3d(-650.0, -150.0, -900.0);
	glVertex3d(-650.0, -150.0, -200.0);
	glVertex3d(650.0, -150.0, -200.0);
	glVertex3d(650.0, -150.0, -900.0);
	glEnd();

	glPointSize(5.0);
	glBegin(GL_POINTS);

	glPushMatrix();
	for (float i = 0; i <= 180; i++) {
		float angle = i * 3.141592f / 180;
		glVertex3d(cos(angle) * 300.0, -150.0, sin(angle) * 300.0 - 200.0);
	}

	glPopMatrix();

	glEnd();

		glColor3d(D.Red, D.Green, D.Blue);
		glTranslated(0.0, -150.0, 0.0);
		glRotated(D.RotateValue, D.Rotate_X, 0.0f, 0.0f);
		glRectd(-D.xPos, -D.yPos, D.xPos, D.yPos - 500.0);

		glRectd(-D.xPos, D.yPos - 500.0, D.xPos - 1800.0, D.yPos);
		glRectd(-D.xPos + 200.0, D.yPos - 500.0, D.xPos - 1600.0, D.yPos);
		glRectd(-D.xPos + 400.0, D.yPos - 500.0, D.xPos - 1400.0, D.yPos);
		glRectd(-D.xPos + 600.0, D.yPos - 500.0, D.xPos - 1200.0, D.yPos);
		glRectd(-D.xPos + 800.0, D.yPos - 500.0, D.xPos - 1000.0, D.yPos);
		glRectd(-D.xPos + 1000.0, D.yPos - 500.0, D.xPos - 800.0, D.yPos);
		glRectd(-D.xPos + 1200.0, D.yPos - 500.0, D.xPos - 600.0, D.yPos);
		glRectd(-D.xPos + 1400.0, D.yPos - 500.0, D.xPos - 400.0, D.yPos);
		glRectd(-D.xPos + 1600.0, D.yPos - 500.0, D.xPos - 200.0, D.yPos);
		glRectd(-D.xPos + 1800.0, D.yPos - 500.0, D.xPos, D.yPos);


	glPopMatrix();	// �ٴ�
}