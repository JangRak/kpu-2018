#pragma once
#include "SharedData.h"
class Kicker
{
public:
	Kicker();
	~Kicker();
public:
	void Init(double X);
	void Draw();
	void Time();
	void Shoot();
	void Set_Main_Foot(int state);
	double Get_X();
	double Get_Y();
	double Get_Z();

	double Get_Move_Z() const { return Move_Z; }

	int Get_Situation() const { return Situation; }

	
private:
	Data D;
	int Main_Foot;
	double Left_Upper_Leg_Rotate_X;
	double Right_Upper_Leg_Rotate_X;
	double Left_Leg_Rotate_X;
	double Right_Leg_Rotate_X;
	double RightHand_Rotate_X;
	double LeftHand_Rotate_X; // �� �յ�
	double Hand_Rotate_X;
	double LeftHand_Rotate_Z; //����κ� �� �󸶳� ��������
	double RightHand_Rotate_Z; //����κ� �� �󸶳� ��������
	double Move_Z;
	int Situation;
};