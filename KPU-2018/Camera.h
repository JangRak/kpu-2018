class Camera
{
public:
	Camera() : EyeX(0.0), EyeY(100.0), EyeZ(500.0),CenterX(0.0), CenterY(-10.0), CenterZ(0.0), UpX(0.0), UpY(1.0), UpZ(0.0) {}
	~Camera() {}

public:
	double GetEyeX() { return EyeX; }
	double GetEyeY() { return EyeY; }
	double GetEyeZ() { return EyeZ; }

	double GetCenterX() { return CenterX; }
	double GetCenterY() { return CenterY; }
	double GetCenterZ() { return CenterZ; }

	double GetUpX() { return UpX; }
	double GetUpY() { return UpY; }
	double GetUpZ() { return UpZ; }

	void SetEyeZ(double EyeZ) { this->EyeZ -= EyeZ; }

private:
	double EyeX;
	double EyeY;
	double EyeZ;

	double CenterX;
	double CenterY;
	double CenterZ;

	double UpX;
	double UpY;
	double UpZ;
};