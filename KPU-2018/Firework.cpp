#include "Firework.h"

enum BEHAVIOR { NONE, SHOOT, BOOM };

Firework::Firework() : D(rand() % 600 - 300, 0, -700.0), Situation(NONE), Counter_Limit(rand() % 200 + 100), Up_Counter(0), Gravity(100.0), Color_Num(rand() % 7), Ban_G(5.0)
{
}

Firework::~Firework()
{
}


void Firework::Init() {
	D.xPos = rand() % 600 - 300;
	D.yPos = 0;
	D.zPos = -700.0;
	Situation = NONE;
	Counter_Limit = rand() % 200 + 100;
	Up_Counter = 0;
	Gravity = 100.0;
	Color_Num = rand() % 7;
	Ban_G = 5.0;
}

void Firework::Start_Fire() {
	Situation = SHOOT;
}

void Firework::Draw() {
	if (Situation == SHOOT) {
		glPushMatrix();
		glTranslated(D.xPos, D.yPos, D.zPos);
		switch (Color_Num) {
		case 0:
			glColor3d(1.0, 0.0, 0.0);
			break;
		case 1:
			glColor3d(0.0, 1.0, 0.0);
			break;
		case 2:
			glColor3d(1.0, 1.0, 0.0);
			break;
		case 3:
			glColor3d(0.5, 0.5, 0.0);
			break;
		case 4:
			glColor3d(0.0, 0.0, 1.0);
			break;
		case 5:
			glColor3d(0.0, 1.0, 1.0);
			break;
		case 6:
			glColor3d(1.0, 0.0, 1.0);
			break;
		}
		glutSolidCube(10.0);
		glPopMatrix();
	}
	else if (Situation == BOOM) {
		glPushMatrix();
		glTranslated(D.xPos, D.yPos, D.zPos);
		for (int i = 0; i < 180.0; i += 15) {
			glPushMatrix();
			glRotated(i, 1.0, 0.0, 0.0);
			for (int j = 0; j < 360; j += 15) {
				glPushMatrix();
				glRotated(j, 0, 0, 1);
				switch (rand() % 6) {
				case 0:
					glColor3d(0.0, 0.0, 0.0);
					break;
				case 1:
					glColor3d(1.0, 0.0, 0.0);
					break;
				case 2:
					glColor3d(1.0, 1.0, 0.0);
					break;
				case 3:
					glColor3d(0.5, 0.5, 0.0);
					break;
				case 4:
					glColor3d(0.0, 1.0, 0.0);
					break;
				case 5:
					glColor3d(0.0, 1.0, 1.0);
				}
				glTranslated(Ban_G, 0, 0);
				glutSolidCube(10.0);
				glPopMatrix();
			}
			glPopMatrix();
		}
		glPopMatrix();
	}
}
void Firework::Time() {
	if (Situation == SHOOT) {
		Up_Counter++;
		D.yPos += (0.04 * Gravity);
		if (Gravity > 0) {
			Gravity -= 0.4;
		}
		if (Up_Counter >= Counter_Limit) {
			Situation = BOOM;
			Up_Counter = 0;
			Counter_Limit = rand() % 50 + 300;
			Gravity = 1.0;
		}
	}
	else if (Situation == BOOM) {
		Up_Counter++;
		Ban_G += 1.2 * Gravity;
		if (Gravity > 0) {
			Gravity -= 0.002;
		}
		if (Up_Counter >= Counter_Limit) {
			Situation = NONE;
			Init();
		}
	}
}