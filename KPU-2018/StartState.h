#pragma once
#include <fstream>
#include "SharedData.h"

class StartState
{
public:
	StartState();
	~StartState();

public:
	GLubyte * LoadDIBitmap(const char *filename, BITMAPINFO **info);

	void Draw();

private:
	GLubyte *pBytes; // 데이터를 가리킬 포인터
	BITMAPINFO *info; // 비트맵 헤더 저장할 변수

	GLuint textures[1];

	double xPos;
	double yPos;
	double zPos;
};