#pragma once
#include "SharedData.h"

class GoalPost
{
public:
	GoalPost(double xPos, double yPos, double zPos, double Rotate_X, double Rotate_Y, double Rotate_Z, bool Direction_Check);
	~GoalPost();

public :
	void Draw();

private :
	bool Direction_Check;
	Data D;
};

