#pragma once
#include "SharedData.h"

class GoalNet
{
public:
	GoalNet(double xPos, double yPos, double zPos, double Rotate_X, double Rotate_Y);
	~GoalNet();

public :
	double get_xPos() const;
	double get_yPos() const;
	double get_zPos() const;
	
	int Get_Collision() const;

	void Collide_Check_back(double xPos, double yPos, double zPos);
	void Collide_Response_back(double zPos);

	void Rotate();

	void zPos_Check();
	void Shave_back(int state);
	void Init();

private :
	Data D;

	int Collision;

	double Init_zPos;
	double ShaveCount;

	bool ShaveCheck;
};