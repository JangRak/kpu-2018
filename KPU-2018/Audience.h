#pragma once
#include "SharedData.h"
#include <fstream>

class AUdience
{
public:
	AUdience();
	~AUdience();

public :
	GLubyte * LoadDIBitmap(const char *filename, BITMAPINFO **info);

	void Draw();

private :
	GLubyte *pBytes; // 데이터를 가리킬 포인터
	BITMAPINFO *info; // 비트맵 헤더 저장할 변수

	GLuint textures[3];
};

