#include "GoalNet.h"

enum COLIDE { YES, NO };

GoalNet::GoalNet(double xPos, double yPos, double zPos, double Rotate_X, double Rotate_Y)
	: D(xPos, yPos, zPos, Rotate_X, Rotate_Y), Collision(NO), Init_zPos(zPos), ShaveCheck(rand() % 2), ShaveCount(0.0) {}

GoalNet::~GoalNet() {}

double GoalNet::get_xPos() const { return D.xPos; }
double GoalNet::get_yPos() const { return D.yPos; }
double GoalNet::get_zPos() const { return D.zPos; }

void GoalNet::Rotate() 
{ 
	glTranslated(0.0, 0.0, -720.0);
	glRotated(90.0, D.Rotate_X, D.Rotate_Y, D.Rotate_Z);
}

int GoalNet::Get_Collision() const { return Collision; }

void GoalNet::Collide_Check_back(double xPos, double yPos, double zPos)
{
	if ((D.zPos - 850.0) + 5.0 > zPos && (D.zPos - 850.0) - 5.0 < zPos) {
		if (D.xPos - 40.0 < xPos && D.xPos + 40.0 > xPos &&
			D.yPos - 40.0 < yPos + 100.0 && D.yPos + 40.0 > yPos + 100.0) {
			Collision = YES;
			Collide_Response_back(zPos);
		}
	}
}

void GoalNet::Collide_Response_back(double zPos){ D.zPos = zPos + 200.0; }

void GoalNet::zPos_Check()
{
	if (Collision == YES) {
		if (D.zPos != Init_zPos)
			D.zPos += 1.0;
		if (D.zPos > Init_zPos)
			D.zPos = Init_zPos;
	}
}

void GoalNet::Shave_back(int state)
{
	if (state == YES && Collision == NO) {
		if (ShaveCheck == true)
			D.zPos += 2.0;
		else if (ShaveCheck == false)
			D.zPos -= 2.0;

		if (D.zPos > 30.0)
			ShaveCheck = false;
		else if (D.zPos < -150.0)
			ShaveCheck = true;

	}
}

void GoalNet::Init()
{
	Collision = NO;
	D.zPos = Init_zPos;
	ShaveCheck = rand() % 2;
	ShaveCount = 0.0;
}