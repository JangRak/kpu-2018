#pragma once
#include <GL/freeglut.h>
#include<math.h>
typedef struct DATA
{
	DATA() {}
	DATA(double xPos, double yPos, double RotateValue, double Rotate_X, double Red, double Green, double Blue)	// 바닥 생성자 (7)
		: xPos(xPos), yPos(yPos), RotateValue(RotateValue), Rotate_X(Rotate_X), Red(Red), Green(Green), Blue(Blue) {}

	DATA(double xPos, double yPos, double zPos, double Rotate_X, double Rotate_Y, double Rotate_Z) //골대 생성자(6)
		: xPos(xPos), yPos(yPos), zPos(zPos), Rotate_X(Rotate_X), Rotate_Y(Rotate_Y), Rotate_Z(Rotate_Z), Red(0.8), Green(1.0), Blue(1.0) {}

	DATA(double xPos, double yPos, double zPos, double Rotate_X, double Rotate_Y)	// 골네트 생성자(5)
		: xPos(xPos), yPos(yPos), zPos(zPos), Rotate_X(Rotate_X), Rotate_Y(Rotate_Y) {}

	DATA(double xPos, double yPos, double zPos, double Rotate_Z)	// 골키퍼 생성자
		: xPos(xPos), yPos(yPos), zPos(zPos), Rotate_Z(Rotate_Z) {}

	DATA(double xPos, double yPos, double zPos)						// 공 생성자(3)
		: xPos(xPos), yPos(yPos), zPos(zPos), RotateValue(0.0), Rotate_X(0.0), Rotate_Y(0.0), Rotate_Z(0.0), Red(1.0), Green(1.0), Blue(1.0) {}



	double RotateValue;
	double Rotate_X;
	double Rotate_Y;
	double Rotate_Z;

	double xPos;
	double yPos;
	double zPos;

	double Red;
	double Green;
	double Blue;
}Data;