#pragma once
#include "SharedData.h"
class Firework
{
public:
	Firework();
	~Firework();
public:
	void Draw();
	void Time();
	void Start_Fire();
	void Init();

private:
	Data D;
	int Counter_Limit;
	int Situation;
	int Up_Counter;
	double Gravity;
	double Ban_G;
	int Color_Num;
};
