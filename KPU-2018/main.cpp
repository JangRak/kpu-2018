#include <GL/freeglut.h>
#include <math.h>
#include <random>
#include <iostream>

#include "Header.h"

using namespace std;

#pragma comment(lib, "winmm.lib")

#include <mmsystem.h>
#include "Digitalv.h"

MCI_OPEN_PARMS mciOpen;
MCI_PLAY_PARMS mciPlay;

DWORD m_dwDeviceID;
static int dwID;

enum VIEW { BASIC, KEEPER, KICKER, BALL };
enum SPINNING { SPIN, NONSPIN, NONE_SELECT_SPINNING };
enum DIRECTION { LEFT, CENTER, RIGHT };
enum FOOT { LEFT_FOOT, RIGHT_FOOT, NONE_SELECT_FOOT };
enum BEHAVIOR_KEEPER { IDLE_KEEPER, DIVING, MOVE, JUMP, DIVING_UP };
enum WORK { ON, OFF, NONE };
enum COLIDE { YES, NO };
enum BEHAVIOR { IDLE_KICKER, ONESTEP, TWOSTEP, SHOOT, RETURN };

const double Width = 800.0;
const double Height = 600.0;

int Main_Window;
int view = 0;

bool Audience = false;
bool kick = false;
bool start;

GLvoid DrawScene(GLvoid);
GLvoid Reshape(int w, int h);
void TimerFunction(int value);
void Keyboard(unsigned char key, int x, int y);
void SpecialKeyboard(int key, int x, int y);
void KeyboardUp(unsigned char key, int x, int y);

Ground ground;
GoalKeeper goalkeeper;
Ball ball;
Kicker kicker;
Firework firework[4];
StartState startstate;
//AUdience audience;

int fire_num = 0;
int fire_count = 0;

GoalPost goalpost[4] = {
			{ 300.0, 200.0, 100.0, 1.0, 0.0, 0.0, false }, // 앞에 기둥
			{ 300.0, 200.0, -100.0, 1.0, 0.0, 0.0, false }, // 뒤에 기둥 
			{ 300.0, 200.0, -100.0, 0.0, 0.0, 1.0, false }, // 좌우 지붕
			{ -300.0, 600.0, 100.0, 0.0, 1.0, 0.0, true } }; // 앞뒤 지붕

Camera camera;

GoalNet goalnet_top[21] = {
			{-290.0, 100.0, -180.0, 1.0, 0.0}, {-290.0, 0.0, -180.0, 1.0, 0.0}, {-290.0, -100.0, -180.0, 1.0, 0.0},
			{-200.0, 100.0, -180.0, 1.0, 0.0}, {-200.0, 0.0, -180.0, 1.0, 0.0}, {-200.0, -100.0, -180.0, 1.0, 0.0},
			{-100.0, 100.0, -180.0, 1.0, 0.0}, {-100.0, 0.0, -180.0, 1.0, 0.0}, {-100.0, -100.0, -180.0, 1.0, 0.0},
			{	0.0, 100.0, -180.0, 1.0, 0.0}, {   0.0, 0.0, -180.0, 1.0, 0.0},	{	0.0, -100.0, -180.0, 1.0, 0.0},
			{ 100.0, 100.0, -180.0, 1.0, 0.0}, { 100.0, 0.0, -180.0, 1.0, 0.0}, { 100.0, -100.0, -180.0, 1.0, 0.0},
			{ 200.0, 100.0, -180.0, 1.0, 0.0}, { 200.0, 0.0, -180.0, 1.0, 0.0}, { 200.0, -100.0, -180.0, 1.0, 0.0},
			{ 290.0, 100.0, -180.0, 1.0, 0.0}, { 290.0, 0.0, -180.0, 1.0, 0.0}, { 290.0, -100.0, -180.0, 1.0, 0.0} };

GoalNet goalnet_left[9] = {
			{ -90.0, 240.0, -120.0, 0.0, 1.0 },{ -90.0, 150.0, -120.0, 0.0, 1.0 },{ -90.0, 50.0, -120.0, 0.0, 1.0 },
			{	0.0, 240.0, -120.0, 0.0, 1.0 },{   0.0, 150.0, -120.0, 0.0, 1.0 },{	  0.0, 50.0, -120.0, 0.0, 1.0 },
			{  90.0, 240.0, -120.0, 0.0, 1.0 },{  90.0, 150.0, -120.0, 0.0, 1.0 },{  90.0, 50.0, -120.0, 0.0, 1.0 } };

GoalNet goalnet_right[9] = {
			{ -90.0, 240.0, -140, 0.0, 1.0 },{ -90.0, 150, -140, 0.0, 1.0 },{ -90.0, 50.0, -140, 0.0, 1.0 },
			{	0.0, 240.0, -140, 0.0, 1.0 },{	 0.0, 150, -140, 0.0, 1.0 },{	0.0, 50.0, -140, 0.0, 1.0 },
			{  90.0, 240.0, -140, 0.0, 1.0 },{	90.0, 150, -140, 0.0, 1.0 },{  90.0, 50.0, -140, 0.0, 1.0 } };

GoalNet goalnet_back[96] = {
			{-290.0, 150.0, -50.0, 0.0, 0.0}, {-290.0, 110.0, -50.0, 0.0, 0.0}, {-290.0, 70.0, -50.0, 0.0, 0.0}, {-290.0, 30.0, -50.0, 0.0, 0.0}, {-290.0, -10.0, -50.0, 0.0, 0.0}, {-290.0, -50.0, -50.0, 0.0, 0.0},
			{-260.0, 150.0, -50.0, 0.0, 0.0}, {-260.0, 110.0, -50.0, 0.0, 0.0}, {-260.0, 70.0, -50.0, 0.0, 0.0}, {-260.0, 30.0, -50.0, 0.0, 0.0}, {-260.0, -10.0, -50.0, 0.0, 0.0}, {-260.0, -50.0, -50.0, 0.0, 0.0},
			{-220.0, 150.0, -50.0, 0.0, 0.0}, {-220.0, 110.0, -50.0, 0.0, 0.0}, {-220.0, 70.0, -50.0, 0.0, 0.0}, {-220.0, 30.0, -50.0, 0.0, 0.0}, {-220.0, -10.0, -50.0, 0.0, 0.0}, {-220.0, -50.0, -50.0, 0.0, 0.0},
			{-180.0, 150.0, -50.0, 0.0, 0.0}, {-180.0, 110.0, -50.0, 0.0, 0.0}, {-180.0, 70.0, -50.0, 0.0, 0.0}, {-180.0, 30.0, -50.0, 0.0, 0.0}, {-180.0, -10.0, -50.0, 0.0, 0.0}, {-180.0, -50.0, -50.0, 0.0, 0.0},
			{-140.0, 150.0, -50.0, 0.0, 0.0}, {-140.0, 110.0, -50.0, 0.0, 0.0}, {-140.0, 70.0, -50.0, 0.0, 0.0}, {-140.0, 30.0, -50.0, 0.0, 0.0}, {-140.0, -10.0, -50.0, 0.0, 0.0}, {-140.0, -50.0, -50.0, 0.0, 0.0},
			{-100.0, 150.0, -50.0, 0.0, 0.0}, {-100.0, 110.0, -50.0, 0.0, 0.0}, {-100.0, 70.0, -50.0, 0.0, 0.0}, {-100.0, 30.0, -50.0, 0.0, 0.0}, {-100.0, -10.0, -50.0, 0.0, 0.0}, {-100.0, -50.0, -50.0, 0.0, 0.0},
			{ -60.0, 150.0, -50.0, 0.0, 0.0}, { -60.0, 110.0, -50.0, 0.0, 0.0}, { -60.0, 70.0, -50.0, 0.0, 0.0}, { -60.0, 30.0, -50.0, 0.0, 0.0}, { -60.0, -10.0, -50.0, 0.0, 0.0}, { -60.0, -50.0, -50.0, 0.0, 0.0},
			{ -20.0, 150.0, -50.0, 0.0, 0.0}, { -20.0, 110.0, -50.0, 0.0, 0.0}, { -20.0, 70.0, -50.0, 0.0, 0.0}, { -20.0, 30.0, -50.0, 0.0, 0.0}, { -20.0, -10.0, -50.0, 0.0, 0.0}, { -20.0, -50.0, -50.0, 0.0, 0.0},
			{  20.0, 150.0, -50.0, 0.0, 0.0}, {  20.0, 110.0, -50.0, 0.0, 0.0}, {  20.0, 70.0, -50.0, 0.0, 0.0}, {  20.0, 30.0, -50.0, 0.0, 0.0}, {  20.0, -10.0, -50.0, 0.0, 0.0}, {  20.0, -50.0, -50.0, 0.0, 0.0},
			{  60.0, 150.0, -50.0, 0.0, 0.0}, {  60.0, 110.0, -50.0, 0.0, 0.0}, {  60.0, 70.0, -50.0, 0.0, 0.0}, {  60.0, 30.0, -50.0, 0.0, 0.0}, {  60.0, -10.0, -50.0, 0.0, 0.0}, {  60.0, -50.0, -50.0, 0.0, 0.0},
			{ 100.0, 150.0, -50.0, 0.0, 0.0}, { 100.0, 110.0, -50.0, 0.0, 0.0}, { 100.0, 70.0, -50.0, 0.0, 0.0}, { 100.0, 30.0, -50.0, 0.0, 0.0}, { 100.0, -10.0, -50.0, 0.0, 0.0}, { 100.0, -50.0, -50.0, 0.0, 0.0},
			{ 140.0, 150.0, -50.0, 0.0, 0.0}, { 140.0, 110.0, -50.0, 0.0, 0.0}, { 140.0, 70.0, -50.0, 0.0, 0.0}, { 140.0, 30.0, -50.0, 0.0, 0.0}, { 140.0, -10.0, -50.0, 0.0, 0.0}, { 140.0, -50.0, -50.0, 0.0, 0.0},
			{ 180.0, 150.0, -50.0, 0.0, 0.0}, { 180.0, 110.0, -50.0, 0.0, 0.0}, { 180.0, 70.0, -50.0, 0.0, 0.0}, { 180.0, 30.0, -50.0, 0.0, 0.0}, { 180.0, -10.0, -50.0, 0.0, 0.0}, { 180.0, -50.0, -50.0, 0.0, 0.0},
			{ 220.0, 150.0, -50.0, 0.0, 0.0}, { 220.0, 110.0, -50.0, 0.0, 0.0}, { 220.0, 70.0, -50.0, 0.0, 0.0}, { 220.0, 30.0, -50.0, 0.0, 0.0}, { 220.0, -10.0, -50.0, 0.0, 0.0}, { 220.0, -50.0, -50.0, 0.0, 0.0},
			{ 260.0, 150.0, -50.0, 0.0, 0.0}, { 260.0, 110.0, -50.0, 0.0, 0.0}, { 260.0, 70.0, -50.0, 0.0, 0.0}, { 260.0, 30.0, -50.0, 0.0, 0.0}, { 260.0, -10.0, -50.0, 0.0, 0.0}, { 260.0, -50.0, -50.0, 0.0, 0.0},
			{ 290.0, 150.0, -50.0, 0.0, 0.0}, { 290.0, 110.0, -50.0, 0.0, 0.0}, { 290.0, 70.0, -50.0, 0.0, 0.0}, { 290.0, 30.0, -50.0, 0.0, 0.0}, { 290.0, -10.0, -50.0, 0.0, 0.0}, { 290.0, -50.0, -50.0, 0.0, 0.0} };

double ViewRotateY = 0.0;
double ViewRotateX = 0.0;

void SetRC()
{

}

void main(int argc, char* argv[])
{
	mciSendCommandW(dwID, MCI_CLOSE, 0, NULL);
	mciOpen.lpstrElementName = "Champions.mp3"; // 파일 경로 입력
	mciOpen.lpstrDeviceType = "mpegvideo";

	mciSendCommand(NULL, MCI_OPEN, MCI_OPEN_ELEMENT | MCI_OPEN_TYPE, (DWORD)(LPVOID)&mciOpen);
	dwID = mciOpen.wDeviceID;
	mciSendCommand(dwID, MCI_PLAY, MCI_DGV_PLAY_REPEAT, (DWORD)(LPVOID)&mciPlay);
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowPosition(512, -400);
	glutInitWindowSize(1024, 800);
	Main_Window = glutCreateWindow("KPU - 2018");
	glutTimerFunc(1, TimerFunction, 1);
	glutDisplayFunc(DrawScene);
	glutReshapeFunc(Reshape);
	glutKeyboardFunc(Keyboard);
	glutKeyboardUpFunc(KeyboardUp);
	glutSpecialFunc(SpecialKeyboard);
	
	glutMainLoop();
}

GLvoid DrawScene(GLvoid)
{
	GLfloat ctrlpoints_top[7][3][3];
	for (int i = 0; i < 7; ++i) {
		for (int j = 0; j < 3; ++j) {
			ctrlpoints_top[i][j][0] = goalnet_top[i * 3 + j].get_xPos();
			ctrlpoints_top[i][j][1] = goalnet_top[i * 3 + j].get_yPos();
			ctrlpoints_top[i][j][2] = goalnet_top[i * 3 + j].get_zPos();
		}
	}

	GLfloat ctrlpoints_left[3][3][3];
	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < 3; ++j) {
			ctrlpoints_left[i][j][0] = goalnet_left[i * 3 + j].get_xPos();
			ctrlpoints_left[i][j][1] = goalnet_left[i * 3 + j].get_yPos();
			ctrlpoints_left[i][j][2] = goalnet_left[i * 3 + j].get_zPos();
		}
	}

	GLfloat ctrlpoints_right[3][3][3];
	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < 3; ++j) {
			ctrlpoints_right[i][j][0] = goalnet_right[i * 3 + j].get_xPos();
			ctrlpoints_right[i][j][1] = goalnet_right[i * 3 + j].get_yPos();
			ctrlpoints_right[i][j][2] = goalnet_right[i * 3 + j].get_zPos();
		}
	}

	GLfloat ctrlpoints_back[16][6][3];
	for (int i = 0; i < 16; ++i) {
		for (int j = 0; j < 6; ++j) {
			ctrlpoints_back[i][j][0] = goalnet_back[i * 6 + j].get_xPos();
			ctrlpoints_back[i][j][1] = goalnet_back[i * 6 + j].get_yPos();
			ctrlpoints_back[i][j][2] = goalnet_back[i * 6 + j].get_zPos();
		}
	}

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // 윈도우, 깊이 버퍼 클리어 하기
	glMatrixMode(GL_MODELVIEW);

	// 필요한 변환 적용 및 그리기 
	//--- 변환을 적용하기 위해서 
	// glPushMatrix 함수를 호출하여 기존의 좌표 시스템을 저장   
	// 필요한 경우 행렬 초기화 ( glLoadIdentity ( ); ) 
	// 변환 적용: 이동, 회전, 신축 등 모델에 적용 할 변환 함수를 호출한다. 
	// 변환이 끝난 후에는 원래의 좌표시스템을 다시 저장하기 위하여 glPopMatrix 함수 호출 
	// 필요한 그리기 작업을 수행한다.

	// 관측 변환: 카메라의 위치 설정 (필요한 경우, 다른 곳에 설정 가능) 

	glPushMatrix();
		if (start == false)
			startstate.Draw();

		else if (start == true) {
			glPointSize(10.0);
			glLineWidth(10.0);

			if (view == BASIC) {
				gluLookAt(camera.GetEyeX(), camera.GetEyeY(), camera.GetEyeZ(), camera.GetCenterX(), camera.GetCenterY(), camera.GetCenterZ(), camera.GetUpX(), camera.GetUpY(), camera.GetUpZ());
				glRotated(ViewRotateX, 1.0, 0.0, 0.0);
				glRotated(ViewRotateY, 0.0, 1.0, 0.0);
				//glRotated(ViewRotateZ, 0.0, 0.0, 1.0);
			}
			else if (view == KEEPER) {
				gluLookAt(goalkeeper.Get_X(), goalkeeper.Get_Y(), goalkeeper.Get_Z() + 1020.0, goalkeeper.Get_X(), goalkeeper.Get_Y(), goalkeeper.Get_Z() + 1021.0, camera.GetUpX(), camera.GetUpY(), camera.GetUpZ());
			}
			else if (view == KICKER) {
				gluLookAt(kicker.Get_X() - cos(30) * kicker.Get_Move_Z(), kicker.Get_Y() + 50.0, kicker.Get_Z() - sin(30.0) * kicker.Get_Move_Z() - 1030.0, kicker.Get_X() - cos(30) * kicker.Get_Move_Z(), kicker.Get_Y() + 50.0, kicker.Get_Z() - sin(30.0) * kicker.Get_Move_Z() - 2000.0, camera.GetUpX(), camera.GetUpY(), camera.GetUpZ());
			}
			else if (view == BALL) {
				gluLookAt(ball.Get_xPos(), ball.Get_yPos() + 10.0 , ball.Get_zPos() - 1020.0, ball.Get_xPos(), ball.Get_yPos() + 10.0, ball.Get_zPos() - 2000.0, camera.GetUpX(), camera.GetUpY(), camera.GetUpZ());
			}

			glColor3d(1.0, 1.0, 1.0);
			glutWireSphere(3000.0, 10.0, 10.0);	

			//audience.Draw();

			glColor3d(1.0, 1.0, 1.0);
			// 골대 그리기
			for (int i = 0; i < 4; ++i)
				goalpost[i].Draw();

			// 공 그리기
			ball.Draw();
			//std::cout << ball.Get_Shooting_Power() << std::endl;
			//std::cout << ball.Get_Time() << std::endl;

			// 키커 그리기
			kicker.Draw();

			glColor3d(1.0, 1.0, 1.0);
			// 그물 위
			glPushMatrix();
			glTranslated(0.0, -130.0, -80.0);   // y축 -130, z축 -80만큼 이동(충돌체크할때 확인하기)
			goalnet_top[0].Rotate();

			glMap2f(GL_MAP2_VERTEX_3, 0.0, 1.0, 3, 3, 0.0, 1.0, 9, 7, &ctrlpoints_top[0][0][0]);
			glEnable(GL_MAP2_VERTEX_3);
			// 그리드를 이용한 곡면 드로잉
			glMapGrid2f(10, 0.0, 1.0, 10, 0.0, 1.0);
			// 선을 이용하여 그리드 연결 
			glEvalMesh2(GL_LINE, 0, 10, 0, 10);
			glPointSize(2.0);
			glPopMatrix();

			// 그물 좌
			glPushMatrix();
			glTranslated(-180.0, -200.0, -80.0); // x축 -180, y축 -200, x축 -80만큼 이동(충돌체크할때 확인하기)
			goalnet_left[0].Rotate();

			glMap2f(GL_MAP2_VERTEX_3, 0.0, 1.0, 3, 3, 0.0, 1.0, 9, 3, &ctrlpoints_left[0][0][0]);
			glEnable(GL_MAP2_VERTEX_3);
			// 그리드를 이용한 곡면 드로잉
			glMapGrid2f(10, 0.0, 1.0, 10, 0.0, 1.0);
			// 선을 이용하여 그리드 연결 
			glEvalMesh2(GL_LINE, 0, 10, 0, 10);
			glPopMatrix();

			// 그물 우
			glPushMatrix();
			glTranslated(430.0, -200.0, -80.0); // x축 430, y축 -200, z축 -80만큼 이동(충돌체크할때 확인하기)
			goalnet_right[0].Rotate();

			glMap2f(GL_MAP2_VERTEX_3, 0.0, 1.0, 3, 3, 0.0, 1.0, 9, 3, &ctrlpoints_right[0][0][0]);
			glEnable(GL_MAP2_VERTEX_3);
			// 그리드를 이용한 곡면 드로잉
			glMapGrid2f(10, 0.0, 1.0, 10, 0.0, 1.0);
			// 선을 이용하여 그리드 연결 
			glEvalMesh2(GL_LINE, 0, 10, 0, 10);
			glPopMatrix();

			// 그물 뒤
			glPushMatrix();
			glTranslated(0.0, -100.0, -850.0);   // y축 -100, z축 -850만큼 이동(충돌체크할때 확인하기)

			glMap2f(GL_MAP2_VERTEX_3, 0.0, 1.0, 3, 6, 0.0, 1.0, 18, 16, &ctrlpoints_back[0][0][0]);
			glEnable(GL_MAP2_VERTEX_3);
			// 그리드를 이용한 곡면 드로잉
			glMapGrid2f(10, 0.0, 1.0, 10, 0.0, 1.0);
			// 선을 이용하여 그리드 연결 
			glEvalMesh2(GL_LINE, 0, 10, 0, 10);
			glPopMatrix();

			// 바닥 그리기
			ground.Draw();   // glTranslated(0.0, -100.0, 0.0)

			goalkeeper.Draw();

			if (ball.Get_Collision_GoalNet() == YES)
				for (int i = 0; i < 4; i++)
					firework[i].Draw();



			glPopMatrix();

			//std::cout << kicker.Get_Situation() << std::endl;
			//std::cout << ball.Get_Can_Shoot() << std::endl;
			//std::cout << kicker.Get_Can_Shoot() << std::endl;
		}
	glutSwapBuffers();
}

GLvoid Reshape(int w, int h)
{
	// 뷰포트 변환 설정: 출력 화면 결정 
	glViewport(0, 0, w, h);
	// 클리핑 변환 설정: 출력하고자 하는 공간 결정 
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0, Width / Height, 1, 8000.0);
	glTranslated(0.0, 0.0, -1000.0);

	//glOrtho(-Width / 2.0, Width / 2.0, -Height / 2.0, Height / 2.0, -400.0, 400.0);

	// 모델링 변환 설정: 디스플레이 콜백 함수에서 모델 변환 적용하기 위하여 Matrix mode 저장 
	glMatrixMode(GL_MODELVIEW);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
}

void TimerFunction(int value)
{
	goalkeeper.Time();

	kicker.Set_Main_Foot(ball.Get_Main_Foot());
	
	kicker.Time();
	if (ball.Get_Kicking_Press() == OFF && ball.Get_Shooting_Power() >= 20.0)
		kicker.Shoot();

	if (kicker.Get_Situation() == RETURN) {
		ball.Set_Can_Shoot(ON);
		if (kick == false) {
			PlaySound(TEXT("Kick.wav"), NULL, SND_FILENAME | SND_ASYNC | SND_ASYNC);
			kick = true;
		}
	}
	else
		kick = false;

	fire_count++;
	if (fire_count == 300) {
		fire_count = 0;
		firework[fire_num++].Start_Fire();
		if (fire_num == 3) {
			fire_num = 0;
		}
	}
	for (int i = 0; i < 4; i++) {
		firework[i].Time();
	}


	if (ball.Get_zPos() < -1000.0 || ball.Get_xPos() > 1000.0 || ball.Get_xPos() < -1000.0) {
		ball.Init();
		kicker.Init(ball.Get_xPos());
		goalkeeper.Init();
	}
	if (ball.Get_BALL_SPEED_PPS() <= 20 && ball.Get_Work() == ON) {
		for (int i = 0; i < 96; ++i)
			goalnet_back[i].Init();
		ball.Init();
		kicker.Init(ball.Get_xPos());
		goalkeeper.Init();
	}

	if (ball.Get_Can_Shoot() == ON && ball.Get_Shooting_Power() > 20.0 &&
		ball.Get_Spinning() != NONE_SELECT_SPINNING && ball.Get_Main_Foot() != NONE_SELECT_FOOT &&
		kicker.Get_Situation() == RETURN) {
		ball.Set_Work(ON);
		ball.Set_Time(0.015);
		ball.Set_Rotate(5.0, ball.Get_Main_Foot());
	}

	if (ball.Get_Kicking_Press() == OFF && (ball.Get_Spinning() == NONE_SELECT_SPINNING || ball.Get_Main_Foot() == NONE_SELECT_FOOT) &&
		ball.Get_Shooting_Power() > 0.0)
		ball.Shooting_Power_Init();

	// 골대랑 체크
	if (ball.Get_zPos() > -705.0 && ball.Get_zPos() < -695.0) {
		// 좌
		if (ball.Get_xPos() > -315.0 && ball.Get_xPos() < -295.0 &&
			ball.Get_yPos() > -50.0 && ball.Get_yPos() < 150.0)
			ball.Set_Collision_Post_Left(YES);
		// 우
		else if (ball.Get_xPos() > 285.0 && ball.Get_xPos() < 315.0 &&
			ball.Get_yPos() > -50.0 && ball.Get_yPos() < 150.0)
			ball.Set_Collision_Post_Right(YES);
		// 상 
		else if (ball.Get_xPos() > -300.0 && ball.Get_xPos() < 300.0 &&
			ball.Get_yPos() > 135.0 && ball.Get_yPos() < 165.0)
			ball.Set_Collision_Post_Up(YES);
	}

	if (ball.Get_zPos() < -645.0 && ball.Get_zPos() > -665.0) {
		if (goalkeeper.Get_Situation() == IDLE_KEEPER || goalkeeper.Get_Situation() == MOVE) {
			if (ball.Get_xPos() > goalkeeper.Get_X() - 40.0 && ball.Get_xPos() < goalkeeper.Get_X() + 40.0 &&
				ball.Get_yPos() > -143.0 && ball.Get_yPos() < -50.0) {
				ball.Set_Collision_Post_Up(YES);
			}
		}
		else if (goalkeeper.Get_Situation() == JUMP) {
			if (goalkeeper.Get_JumpState() == 0) {
				if (ball.Get_xPos() > goalkeeper.Get_X() - 40.0 && ball.Get_xPos() < goalkeeper.Get_X() + 40.0 &&
					ball.Get_yPos() > -143.0 && ball.Get_yPos() < -50.0) {
					ball.Set_Collision_Post_Up(YES);
				}
			}
			else if (goalkeeper.Get_JumpState() == 1 || goalkeeper.Get_JumpState() == 2) {
				if (ball.Get_xPos() > goalkeeper.Get_X() - 45.0 && ball.Get_xPos() < goalkeeper.Get_X() + 45.0 &&
					ball.Get_yPos() > goalkeeper.Get_Y() - 108.0 && ball.Get_yPos() < goalkeeper.Get_Y() + 40.0) {
					ball.Set_Collision_Post_Up(YES);
				}
			}
		}
		else if (goalkeeper.Get_Situation() == DIVING || goalkeeper.Get_Situation() == DIVING_UP) {
			if (goalkeeper.Get_JumpState() == 0) {
				if (ball.Get_xPos() > goalkeeper.Get_X() - 40.0 && ball.Get_xPos() < goalkeeper.Get_X() + 40.0 &&
					ball.Get_yPos() > -143.0 && ball.Get_yPos() < -50.0) {
					ball.Set_Collision_Post_Up(YES);
				}
			}
			else if (goalkeeper.Get_JumpState() == 1 || goalkeeper.Get_JumpState() == 2 || goalkeeper.Get_JumpState() == 3) {
				if (goalkeeper.Get_Dive_Direct() == false) { // 왼쪽

					if (ball.Get_xPos() > goalkeeper.Get_X() + cos((goalkeeper.Get_Zangle() + 120.0) * 3.14 / 180.0) * 108.0 &&
						ball.Get_xPos() < goalkeeper.Get_X() - cos((goalkeeper.Get_Zangle() + 120.0) * 3.14 / 180.0) * 108.0 &&
						ball.Get_yPos() < goalkeeper.Get_Y() + 60.0&&
						ball.Get_yPos() > goalkeeper.Get_Y() - 60.0) {
						ball.Set_Collision_Post_Left(YES);
					}
				}
				else { // 오른쪽
					if (ball.Get_xPos() < goalkeeper.Get_X() + cos((goalkeeper.Get_Zangle() + 70.0) * 3.14 / 180.0) * 108.0 &&
						ball.Get_xPos() > goalkeeper.Get_X() - cos((goalkeeper.Get_Zangle() + 70.0) * 3.14 / 180.0) * 108.0 &&
						ball.Get_yPos() < goalkeeper.Get_Y() + 60.0&&
						ball.Get_yPos() > goalkeeper.Get_Y() - 60.0) {
						ball.Set_Collision_Post_Right(YES);
					}
				}
			}
		}
	}

	// 골네트랑 체크
	for (int i = 0; i < 96; ++i) {
		if (goalnet_back[i].get_yPos() != 150.0 && goalnet_back[i].get_yPos() != -50.0 &&
			goalnet_back[i].get_xPos() != -290.0 && goalnet_back[i].get_xPos() != 290.0)
			goalnet_back[i].Collide_Check_back(ball.Get_xPos(), ball.Get_yPos(), ball.Get_zPos());
		if (goalnet_back[i].Get_Collision() == YES)
			ball.Set_Collision_GoalNet(YES);
		goalnet_back[i].zPos_Check();

		if (goalnet_back[i].get_yPos() != 100.0 && goalnet_back[i].get_yPos() != -50.0 &&
			goalnet_back[i].get_xPos() != 290.0 && goalnet_back[i].get_xPos() != -290.0) {
			goalnet_back[i].Shave_back(ball.Get_Collision_GoalNet());
		}
	}

	if (ball.Get_Collision_GoalNet() == YES) {
		if (Audience == false) {
			PlaySound(TEXT("Audience.wav"), NULL, SND_FILENAME | SND_ASYNC | SND_ASYNC);
			Audience = true;
		}
	}
	else {
		Audience = false;
	}

	//for (int i = 0; i < 21; ++i) {
	//	goalnet_left[i].get_xPos
	//}

	glutPostRedisplay(); // 화면 재출력을 위하여 디스플레이 콜백 함수 호출 
	glutTimerFunc(1, TimerFunction, 1);
}
void Keyboard(unsigned char key, int x, int y)
{
	switch (key) {
	case 'y' :
		ViewRotateY += 15.0;
		break;
	case 'x':
		ViewRotateX -= 10.0;
		break;
	case '-' :
		camera.SetEyeZ(-10.0);
		break;
	case'i':
		goalkeeper.Init();
		break;
	case 'w':
		goalkeeper.Jump();
		break;
	case 'a':
		if (view == KEEPER)
			goalkeeper.Move_Right();
		else
			goalkeeper.Move_Left();
		break;
	case 'd':
		if (view == KEEPER)
			goalkeeper.Move_Left();
		else
			goalkeeper.Move_Right();
		break;
	case 'q':
		//if (kicker.Get_Situation() == RETURN)
			goalkeeper.Diving_Left();
		break;
	case 'e':
		//if (kicker.Get_Situation() == RETURN)
			goalkeeper.Diving_Right();
		break;

	// 키커 키
	case '/' :
		ball.Set_Shooting_Power();
		ball.Set_Kicking_Press(ON);
		break;
	case 'l' :
		ball.Set_Main_Foot(RIGHT_FOOT);
		break;
	case 'k' :
		ball.Set_Main_Foot(LEFT_FOOT);
		break;
	case 'o' :
		ball.Set_Spinning(SPIN);
		break;
	case 'p' :
		ball.Set_Spinning(NONSPIN);
		break;

	case 'n':
		ball.Init();
		kicker.Init(ball.Get_xPos());
		goalkeeper.Init();
		for (int i = 0; i < 96; ++i)
			goalnet_back[i].Init();
		break;

	case 'c' :
		glutDestroyWindow(Main_Window);
		break;
		//뷰변환
	case 'v':
		view = (view + 1) % 4;
		break;
	case 32 :
		start = true;
		break;
	}

	glutPostRedisplay();
}

void KeyboardUp(unsigned char key, int x, int y)
{
	switch (key) {
	case '/':
		ball.Set_Kicking_Press(OFF);
		ball.Update();
		break;
	}
}

void SpecialKeyboard(int key, int x, int y) {
	switch (key) {
	case GLUT_KEY_LEFT:
		ball.Set_Dir_Power(-2.0);
		break;
	case GLUT_KEY_RIGHT:
		ball.Set_Dir_Power(2.0);
		break;
	case GLUT_KEY_UP:

		break;
	case GLUT_KEY_DOWN:

		break;
	}
}