#include "GoalPost.h"

GoalPost::GoalPost(double xPos, double yPos, double zPos, double Rotate_X, double Rotate_Y, double Rotate_Z, bool Direction_Check)
	: D(xPos, yPos, zPos, Rotate_X, Rotate_Y, Rotate_Z), Direction_Check(Direction_Check){}

GoalPost::~GoalPost() {}

void GoalPost::Draw()
{
	glPushMatrix();   // ���
		glTranslated(0.0, 0.0, -800.0);
		glPushMatrix();   //��� �� or �� �׸�
			if (Direction_Check == true) // ��� �� �׸���
				glTranslated(D.xPos, 50.0, -D.zPos);
			else //��� �� �׸���
				glTranslated(D.xPos, 50.0, D.zPos);
			glRotated(90.0, D.Rotate_X, D.Rotate_Y, D.Rotate_Z);
			glColor3d(D.Red, D.Green, D.Blue);
			glutSolidCylinder(10.0, D.yPos, 20.0, 20.0);
			glPopMatrix();   // ��� ��or��

		glPushMatrix();   // ��� �� or �� �׸�
			if (Direction_Check == true) // ��� �� �׸���
				glTranslated(D.xPos, 50.0, D.zPos);
			else // ��� �� �׸���
				glTranslated(-D.xPos, 50.0, D.zPos);
			glRotated(90.0, D.Rotate_X, D.Rotate_Y, D.Rotate_Z);
			glColor3d(D.Red, D.Green, D.Blue);
			glutSolidCylinder(10.0, D.yPos, 20.0, 20.0);
		glPopMatrix();   // ��� ��or��
	glPopMatrix();   // ���
}
