#include "Kicker.h"

enum BEHAVIOR { IDLE_KICKER, ONESTEP, TWOSTEP, SHOOT, RETURN };
enum FOOTLEG { LEFT, RIGHT };
Kicker::Kicker() : Situation(IDLE_KICKER), Main_Foot(RIGHT), Left_Upper_Leg_Rotate_X(0.0), Right_Upper_Leg_Rotate_X(0.0), Left_Leg_Rotate_X(0.0), Right_Leg_Rotate_X(0.0), Move_Z(200.0), LeftHand_Rotate_X(0.0), LeftHand_Rotate_Z(-15.0), RightHand_Rotate_Z(15.0), RightHand_Rotate_X(0.0), Hand_Rotate_X(0.0)
{
	D.xPos = -100.0;
	D.yPos = -130.0;
	D.zPos = 700.0;
	D.Rotate_X = 0.0;
	D.Rotate_Y = 30.0;
}

Kicker::~Kicker() {}

double Kicker::Get_X() { return D.xPos; }
double Kicker::Get_Y() { return D.yPos; }
double Kicker::Get_Z() { return D.zPos; }

void Kicker::Set_Main_Foot(int state) {
	Main_Foot = state;
}

void Kicker::Init(double X) {
	Situation = IDLE_KICKER;
	Main_Foot = RIGHT;
	Left_Upper_Leg_Rotate_X = 0.0;
	Right_Upper_Leg_Rotate_X = 0.0;
	LeftHand_Rotate_X = 0.0;
	LeftHand_Rotate_Z = -15.0;
	Left_Leg_Rotate_X = 0.0;
	Right_Leg_Rotate_X = 0.0;
	RightHand_Rotate_X = 0.0;
	RightHand_Rotate_Z = 15.0;
	Hand_Rotate_X = 0.0;
	Move_Z = 200.0;
	D.Rotate_X = 0.0;
	D.Rotate_Y = 30.0;
	D.zPos = 700.0;
	D.xPos = X;
	D.yPos = -130.0;
}
void Kicker::Shoot() {
	if (Situation == IDLE_KICKER) {
		Situation = ONESTEP;
	}
}

void Kicker::Draw() {
	if (Main_Foot == RIGHT) {
		glPushMatrix();
		glTranslated(D.xPos, D.yPos + 40.0, D.zPos);
		glRotated(-D.Rotate_Y, 0.0, 1.0, 0.0);
		glTranslated(0.0, 0.0, Move_Z);
		glRotated(D.Rotate_X, 1.0, 0.0, 0.0);
		glTranslated(0.0, 40.0, 0.0);

		glPushMatrix();//�Ӹ�
		glColor3d(0.3, 0.3, 0.7);
		glutSolidSphere(10.0, 10, 10);
		glPopMatrix();//�Ӹ�

		glPushMatrix();//��
		glColor3d(1.0, 2.0, 0.0);
		glTranslated(0.0, -9.0, 0.0);
		glRotated(90.0, 1.0, 0.0, 0.0);
		glutSolidCylinder(3.0, 5.0, 10, 10);
		glPopMatrix();//��

		glPushMatrix(); // ��
		glColor3d(0.5, 0.3, 1.0);
		glTranslated(0.0, -33.0, 0.0);
		glScaled(1.5, 2.0, 1.0);
		glutSolidCube(20.0);
		glPopMatrix(); // ��

		glPushMatrix(); // ����
		glColor3d(1.0, 0.0, 0.0);
		glTranslated(-15.0, -15.0, 0.0);
		glRotated(-Hand_Rotate_X, 1.0, 0.0, 0.0);
		glRotated(LeftHand_Rotate_Z, 0.0, 0.0, 1.0);
		glPushMatrix();//���� ������
		glRotated(90.0, 1.0, 0.0, 0.0);
		glutSolidCylinder(4.0, 20.0, 10, 10);
		glPopMatrix();// ���� ������
		glTranslated(0.0, -20.0, 0.0);
		glRotated(LeftHand_Rotate_X, 1.0, 0.0, 0.0);
		glRotated(90.0, 1.0, 0.0, 0.0);
		glutSolidCylinder(4.0, 20.0, 10, 10);
		glPopMatrix(); // ����

		glPushMatrix(); // ������
		glColor3d(1.0, 0.0, 0.0);
		glTranslated(15.0, -15.0, 0.0);
		glRotated(Hand_Rotate_X, 1.0, 0.0, 0.0);
		glRotated(RightHand_Rotate_Z, 0.0, 0.0, 1.0);
		glPushMatrix();//������ ������
		glRotated(90.0, 1.0, 0.0, 0.0);
		glutSolidCylinder(4.0, 20.0, 10, 10);
		glPopMatrix();// ������ ������
		glTranslated(0.0, -20.0, 0.0);
		glRotated(RightHand_Rotate_X, 1.0, 0.0, 0.0);
		glRotated(90.0, 1.0, 0.0, 0.0);
		glutSolidCylinder(4.0, 20.0, 10, 10);
		glPopMatrix(); // ������

		glPushMatrix();//�޴ٸ�
		glColor3d(0.0, 0.8, 0.8);
		glTranslated(-5.0, -53.0, 0.0);
		glRotated(Left_Upper_Leg_Rotate_X, 1.0, 0.0, 0.0);
		glPushMatrix();//�� �����
		glRotated(90.0, 1.0, 0.0, 0.0);
		glutSolidCylinder(4.0, 20.0, 10, 10);
		glPopMatrix(); //�� �����
		glPushMatrix();// �޴ٸ� ���Ƹ�
		glTranslated(0.0, -20.0, 0.0);
		glRotated(90.0 + Left_Leg_Rotate_X, 1.0, 0.0, 0.0);
		glutSolidCylinder(4.0, 20.0, 10, 10);
		glPopMatrix();// �޴ٸ� ���Ƹ�
		glPopMatrix();//�޴ٸ�

		glPushMatrix();//�����ٸ�
		glColor3d(0.0, 0.8, 0.8);
		glTranslated(5.0, -53.0, 0.0);
		glRotated(Right_Upper_Leg_Rotate_X, 1.0, 0.0, 0.0);
		glPushMatrix();//���� �����
		glRotated(90.0, 1.0, 0.0, 0.0);
		glutSolidCylinder(4.0, 20.0, 10, 10);
		glPopMatrix(); //���� �����
		glPushMatrix();//�����ٸ� ���Ƹ�
		glTranslated(0.0, -20.0, 0.0);
		glRotated(90.0 + Right_Leg_Rotate_X, 1.0, 0.0, 0.0);
		glutSolidCylinder(4.0, 20.0, 10, 10);
		glPopMatrix();// �����ٸ� ���Ƹ�
		glPopMatrix();//�����ٸ�
		glPopMatrix();
	}
	else if (Main_Foot == LEFT) {
		glPushMatrix();
		glTranslated(D.xPos, D.yPos + 40.0, D.zPos);
		glRotated(D.Rotate_Y, 0.0, 1.0, 0.0);
		glTranslated(0.0, 0.0, Move_Z);
		glRotated(D.Rotate_X, 1.0, 0.0, 0.0);
		glTranslated(0.0, 40.0, 0.0);

		glPushMatrix();//�Ӹ�
		glColor3d(0.3, 0.3, 0.7);
		glutSolidSphere(10.0, 10, 10);
		glPopMatrix();//�Ӹ�

		glPushMatrix();//��
		glColor3d(1.0, 2.0, 0.0);
		glTranslated(0.0, -9.0, 0.0);
		glRotated(90.0, 1.0, 0.0, 0.0);
		glutSolidCylinder(3.0, 5.0, 10, 10);
		glPopMatrix();//��

		glPushMatrix(); // ��
		glColor3d(0.5, 0.3, 1.0);
		glTranslated(0.0, -33.0, 0.0);
		glScaled(1.5, 2.0, 1.0);
		glutSolidCube(20.0);
		glPopMatrix(); // ��

		glPushMatrix(); // ����
		glColor3d(1.0, 0.0, 0.0);
		glTranslated(-15.0, -15.0, 0.0);
		glRotated(LeftHand_Rotate_X, 1.0, 0.0, 0.0);
		glRotated(LeftHand_Rotate_Z, 0.0, 0.0, 1.0);
		glPushMatrix();//���� ������
		glRotated(90.0, 1.0, 0.0, 0.0);
		glutSolidCylinder(4.0, 20.0, 10, 10);
		glPopMatrix();// ���� ������
		glTranslated(0.0, -20.0, 0.0);
		glRotated(-LeftHand_Rotate_X, 1.0, 0.0, 0.0);
		glRotated(90.0, 1.0, 0.0, 0.0);
		glutSolidCylinder(4.0, 20.0, 10, 10);
		glPopMatrix(); // ����

		glPushMatrix(); // ������
		glColor3d(1.0, 0.0, 0.0);
		glTranslated(15.0, -15.0, 0.0);
		glRotated(RightHand_Rotate_X, 1.0, 0.0, 0.0);
		glRotated(RightHand_Rotate_Z, 0.0, 0.0, 1.0);
		glPushMatrix();//������ ������
		glRotated(90.0, 1.0, 0.0, 0.0);
		glutSolidCylinder(4.0, 20.0, 10, 10);
		glPopMatrix();// ������ ������
		glTranslated(0.0, -20.0, 0.0);
		glRotated(-RightHand_Rotate_X, 1.0, 0.0, 0.0);
		glRotated(90.0, 1.0, 0.0, 0.0);
		glutSolidCylinder(4.0, 20.0, 10, 10);
		glPopMatrix(); // ������

		glPushMatrix();//�޴ٸ�
		glColor3d(0.0, 0.8, 0.8);
		glTranslated(-5.0, -53.0, 0.0);
		glRotated(Left_Upper_Leg_Rotate_X, 1.0, 0.0, 0.0);
		glPushMatrix();//�� �����
		glRotated(90.0, 1.0, 0.0, 0.0);
		glutSolidCylinder(4.0, 20.0, 10, 10);
		glPopMatrix(); //�� �����
		glPushMatrix();// �޴ٸ� ���Ƹ�
		glTranslated(0.0, -20.0, 0.0);
		glRotated(90.0 + Left_Leg_Rotate_X, 1.0, 0.0, 0.0);
		glutSolidCylinder(4.0, 20.0, 10, 10);
		glPopMatrix();// �޴ٸ� ���Ƹ�
		glPopMatrix();//�޴ٸ�

		glPushMatrix();//�����ٸ�
		glColor3d(0.0, 0.8, 0.8);
		glTranslated(5.0, -53.0, 0.0);
		glRotated(Right_Upper_Leg_Rotate_X, 1.0, 0.0, 0.0);
		glPushMatrix();//���� �����
		glRotated(90.0, 1.0, 0.0, 0.0);
		glutSolidCylinder(4.0, 20.0, 10, 10);
		glPopMatrix(); //���� �����
		glPushMatrix();//�����ٸ� ���Ƹ�
		glTranslated(0.0, -20.0, 0.0);
		glRotated(90.0 + Right_Leg_Rotate_X, 1.0, 0.0, 0.0);
		glutSolidCylinder(4.0, 20.0, 10, 10);
		glPopMatrix();// �����ٸ� ���Ƹ�
		glPopMatrix();//�����ٸ�
		glPopMatrix();
	}
}

void Kicker::Time() {
	if (Main_Foot == RIGHT) {
		if (Situation == ONESTEP) {
			double k = 150.0;
			if (Right_Upper_Leg_Rotate_X < 45.0) {
				Move_Z -= 30.0 / k;
				Right_Upper_Leg_Rotate_X += 45.0 / k;
				Right_Leg_Rotate_X -= 45.0 / k;
				Left_Upper_Leg_Rotate_X -= 45.0 / k;
				Left_Leg_Rotate_X -= 45.0 / k;
				Hand_Rotate_X -= 30.0 / k;
				LeftHand_Rotate_X += 30.0 / k;
				RightHand_Rotate_X += 30.0 / k;
				RightHand_Rotate_Z += 10.0 / k;
			}
			else {
				Situation = TWOSTEP;
			}
		}
		else if (Situation == TWOSTEP) {
			double k = 300.0;
			if (Right_Upper_Leg_Rotate_X > -45.0) {
				Move_Z -= 60.0 / k;
				Right_Upper_Leg_Rotate_X -= 90.0 / k;
				Left_Upper_Leg_Rotate_X += 90.0 / k;
				Left_Leg_Rotate_X += 45.0 / k;
				Hand_Rotate_X += 60.0 / k;
				LeftHand_Rotate_Z -= 30.0 / k;
				RightHand_Rotate_Z -= 10.0 / k;
			}
			else {
				Situation = SHOOT;
			}
		}
		else if (Situation == SHOOT) {
			double k = 150.0;
			if (Right_Upper_Leg_Rotate_X <= 45.0) {
				Move_Z -= 20.0 / k;
				Right_Upper_Leg_Rotate_X += 90.0 / k;
				Right_Leg_Rotate_X += 45.0 / k;
				Left_Upper_Leg_Rotate_X -= 20.0 / k;
				Left_Leg_Rotate_X -= 40.0 / k;
				Hand_Rotate_X -= 60.0 / k;

				LeftHand_Rotate_Z += 45.0 / k;
				RightHand_Rotate_Z += 75.0 / k;
			}
			else {
				Situation = RETURN;
			}
		}
		else if (Situation == RETURN) {
			double k = 300.0;
			if (Right_Upper_Leg_Rotate_X >= 0.0) {
				Move_Z -= 10.0 / k;
				Right_Upper_Leg_Rotate_X -= 45.0 / k;
				Left_Upper_Leg_Rotate_X -= 25.0 / k;
				Left_Leg_Rotate_X += 40.0 / k;
				Hand_Rotate_X += 60.0 / k;
				LeftHand_Rotate_Z -= 15.0 / k;
				RightHand_Rotate_Z -= 75.0 / k;
			}
		}
	}
	else if (Main_Foot == LEFT) {
		if (Situation == ONESTEP) {
			double k = 150.0;
			if (Left_Upper_Leg_Rotate_X < 45.0) {
				Move_Z -= 30.0 / k;
				Left_Upper_Leg_Rotate_X += 45.0 / k;
				Left_Leg_Rotate_X -= 45.0 / k;
				Right_Upper_Leg_Rotate_X -= 45.0 / k;
				Right_Leg_Rotate_X -= 45.0 / k;
				Hand_Rotate_X -= 30.0 / k;
				RightHand_Rotate_X += 30.0 / k;
				LeftHand_Rotate_X -= 30.0 / k;
				LeftHand_Rotate_Z -= 10.0 / k;
			}
			else {
				Situation = TWOSTEP;
			}
		}
		else if (Situation == TWOSTEP) {
			double k = 300.0;
			if (Left_Upper_Leg_Rotate_X > -45.0) {
				Move_Z -= 60.0 / k;
				Left_Upper_Leg_Rotate_X -= 90.0 / k;
				Right_Upper_Leg_Rotate_X += 90.0 / k;
				Right_Leg_Rotate_X += 45.0 / k;
				Hand_Rotate_X += 60.0 / k;
				RightHand_Rotate_Z += 30.0 / k;
				LeftHand_Rotate_Z += 10.0 / k;
			}
			else {
				Situation = SHOOT;
			}
		}
		else if (Situation == SHOOT) {
			double k = 150.0;
			if (Move_Z > 90.0) {
				Move_Z -= 20.0 / k;
				Left_Upper_Leg_Rotate_X += 90.0 / k;
				Left_Leg_Rotate_X += 45.0 / k;
				Right_Upper_Leg_Rotate_X -= 20.0 / k;
				Right_Leg_Rotate_X -= 40.0 / k;
				Hand_Rotate_X -= 60.0 / k;
				RightHand_Rotate_Z -= 45.0 / k;
				LeftHand_Rotate_Z -= 75.0 / k;
			}
			else {
				Situation = RETURN;
			}
		}
		else if (Situation == RETURN) {
			double k = 300.0;
			if (Move_Z > 80.0) {
				Move_Z -= 10.0 / k;
				Left_Upper_Leg_Rotate_X -= 45.0 / k;
				Right_Upper_Leg_Rotate_X -= 25.0 / k;
				Right_Leg_Rotate_X += 40.0 / k;
				Hand_Rotate_X += 60.0 / k;
				RightHand_Rotate_Z += 15.0 / k;
				LeftHand_Rotate_Z += 75.0 / k;
			}
		}
	}
}