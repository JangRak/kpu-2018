#pragma once
#include "SharedData.h"
#include <time.h>

class Ball
{
public:
	Ball();
	~Ball();
public:
	void Draw();
	void Init();

	// Set 함수
	void Set_Spinning(int state);

	void Set_xPos(double xPos);
	void Set_yPos(double yPos);
	void Set_zPos(double zPos);

	void Set_Rotate(double RotateValue, int Foot);

	void Set_Dir_Value(double Value);

	void Set_Shooting_Power();
	void Set_Dir_Power(double Value);

	void Set_Can_Shoot(int state) { Can_Shoot = state; }

	// 시간 체크
	void Set_Time(double Value) { Time += Value; }
	// 공차기
	void Set_Work(int state) { Work = state; }
	// 주발
	void Set_Main_Foot(int state) { Main_Foot = state; }
	// 골네트랑 충돌
	void Set_Collision_GoalNet(int state) { Collision_GoalNet = state; }
	// 킥여부
	void Set_Kicking_Press(int state) { Kicking_Press = state; }

	void Shooting_Power_Init();

	void Update();

	void Set_Collision_Post_Up(int state) { Collision_Post_Up = state; }
	void Set_Collision_Post_Left(int state) { Collision_Post_Left = state; }
	void Set_Collision_Post_Right(int state) { Collision_Post_Right = state; }

	// Get 함수
	double Get_xPos() const { return Trans_xPos; }
	double Get_yPos() const { return Trans_yPos; }
	double Get_zPos() const { return Trans_zPos; }

	double Get_Shooting_Power() const { return Shooting_Power; }
	double Get_Trans_xPos() const { return Dir_Power; }
	double Get_Time() const { return Time; }
	int Get_Work() const { return Work; }
	int Get_Main_Foot() const { return Main_Foot; }
	int Get_Collision_GoalNet() const { return Collision_GoalNet; }
	int Get_Kicking_Press() const { return Kicking_Press; }
	int Get_Spinning() const { return Spinning; }
	int Get_Can_Shoot() const{ return Can_Shoot; }

	double Get_BALL_SPEED_PPS() const { return BALL_SPEED_PPS; }
	

private:
	int Spinning;
	int Work;
	int Main_Foot;
	int Collision_GoalNet;
	int Collision_Ground;

	int Collision_Post_Up;
	int Collision_Post_Left;
	int Collision_Post_Right;

	int Kicking_Press;

	int Can_Shoot;

	double Dir_Value;
	double Shooting_Power;
	double Dir_Power;
	double Velocity;

	double Trans_xPos;
	double Trans_yPos;
	double Trans_zPos;

	double xPos_for_Collision_Post;
	double yPos_for_Collision_Post;
	double zPos_for_Collision_Post;

	double xPos_for_Collision_Ground;
	double yPos_for_Collision_Ground;
	double zPos_for_Collision_Ground;

	double Time;
	double Time_for_Collision_Ground;

	const float Gravity = 9.8f;

	Data D;

	double PIXEL_PER_METER;
	double BALL_SPEED_KMPH;
	double BALL_SPEED_MPM;
	double BALL_SPEED_MPS;
	double BALL_SPEED_PPS;

	double DIR_SPEED_KMPH;
	double DIR_SPEED_MPM;
	double DIR_SPEED_MPS;
	double DIR_SPEED_PPS;
};